<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="basePath" value="${pageContext.request.contextPath}" />
<head>
    <%--usually we use utf-8 for charset/encoding--%>
    <%--<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">--%>
    <title>Spring 4 MVC - YES! Index Page</title>
    <link rel="stylesheet" type="text/css" href="${basePath}/static/css/common.css"/>

</head>
<body>

    <%--center tag is deprecated, usually we use css to style the page--%>
    <%--<center>--%>
        <%--<h2>Hello World</h2>--%>

        <%--<h3>--%>
            <%--<a href="home.htm">Click Here</a>--%>
        <%--</h3>--%>
        <%--<!-- 		<h3>--%>
                    <%--<a href="employee">Employee List</a>--%>
                <%--</h3> -->--%>
    <%--</center>--%>

    <div class="container">
        <%--change this to flexBoxH if you want to align horizontally, in CSS2 you need to use float to align element horizontally.--%>
        <%--find more about css float here: https://css-tricks.com/all-about-floats/--%>
        <div class="flexBoxV">
            <h2 class="title">Hello World</h2>
            <h3>
                <a href="home.htm">Click Here</a>
            </h3>
        </div>
    </div>
</body>
</html>