<%--this is standard HTML5 head--%>
<!DOCTYPE html>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:set var="basePath" value="${pageContext.request.contextPath}"/>
<html>
	<head>	
		<link rel="stylesheet" type="text/css" href="${basePath}/static/css/home.css"/>
	    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
		<script type="text/javascript" src="${basePath}/static/js/jquery.bxslider.min.js"></script>
		<link href="${basePath}/static/css/jquery.bxslider.css" rel="stylesheet" />
		<script type="text/javascript" src="${basePath}/static/js/home.js"></script>

		
		<title>TESTAPP</title>
	</head>
	<body>
		<div class = "container">
			<div class = "content">
				<div class = "slider">
					<ul class="bxslider">
						<li><a href="http://www.ezloan.cn" target="_blank"><img src="${basePath}/static/image/slider1.png"/></a></li>
						<li><a href="http://www.ezloan.cn" target="_blank"><img src="${basePath}/static/image/slider2.png"/></a></li>
						<li><a href="http://www.ezloan.cn" target="_blank"><img src="${basePath}/static/image/slider3.jpg"/></a></li>
 					</ul>
				</div>
				<div class="info">
	            	<div class="title">INFORMATION</div>
		            <div class="indoorAndOutDoorWrapper">
		                <ul class="indoor">
		                    <li class="temperature"><c:out value='${indoorQuality.temperature}'/>℃</li>
		                    <li class="humidity"><c:out value='${indoorQuality.humidity}'/>%</li>
		                    <li class="pm2_5"><c:out value='${indoorQuality.particulateMatter}'/></li>
		                    <li class="co2"><c:out value='${indoorQuality.carbonDioxide}'/></li>
		                </ul>
		
		                <ul class="outdoor">
		                    <li class="temperature"><c:out value='${outDoorQuality.temperature}'/>℃</li>
		                    <li class="humidity"><c:out value='${outDoorQuality.humidity}'/>%</li>
		                    <li class="pm2_5"><c:out value='${outDoorQuality.particulateMatter}'/></li>
		                    <li class="co2"><c:out value='${outDoorQuality.carbonDioxide}'/></li>
		                </ul>
		            </div>
	        	</div>
	        	<div class = "adsWrapper">
		        	<div class="ads">
			            <ul class="flexBox">
			                <li class = "adImages"><a href="#" target="_blank"><img src="${basePath}/static/image/ad1.png"/></a></li>
			                <li class = "adImages"><a href="#" target="_blank"><img src="${basePath}/static/image/ad2.png"/></a></li>
			                <li class = "adImages"><a href="#" target="_blank"><img src="${basePath}/static/image/ad3.png"/></a></li>
            			    <li class = "adImages"><a href="#" target="_blank"><img src="${basePath}/static/image/ad4.png"/></a></li>
            			    <li class = "adImages"><a href="#" target="_blank"><img src="${basePath}/static/image/ad5.png"/></a></li>
            			    <li class = "adImages"><a href="#" target="_blank"><img src="${basePath}/static/image/ad6.png"/></a></li>
			            </ul>
		        	</div>
		        </div>
		        <div class = "about">
		        </div>TestApp Created by Jialiang Zhong
			</div>
		</div>
	</body>
</html>