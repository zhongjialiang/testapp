package com.jialiang.testapp.entity;

public class FamilyLifeQuality {
    public final static int in_door = 0;
    public final static int out_door = 1;

    public final static int detector_temperature = 0;
    public final static int detector_humidity = 1;
    public final static int detector_particulateMatter = 2;
    public final static int detector_carbonDioxide = 3;
}
