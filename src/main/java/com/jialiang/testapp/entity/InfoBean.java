package com.jialiang.testapp.entity;

public class InfoBean{
    private int detectorID;
    private float temperature;
    private float humidity;
    private float particulateMatter;
    private float carbonDioxide;
    private int type;	

	public int getDetectorID() {
	    return detectorID;
	}
	
	public void setDetectorID(int detectorID) {
	    this.detectorID = detectorID;
	}
	
	public float getTemperature() {
	    return temperature;
	}
	
	public void setTemperature(float temperature) {
	    this.temperature = temperature;
	}
	
	public float getHumidity() {
	    return humidity;
	}
	
	public void setHumidity(float humidity) {
	    this.humidity = humidity;
	}
	
	public float getParticulateMatter() {
	    return particulateMatter;
	}
	
	public void setParticulateMatter(float particulateMatter) {
	    this.particulateMatter = particulateMatter;
	}
	
	public float getCarbonDioxide() {
	    return carbonDioxide;
	}
	
	public void setCarbonDioxide(float carbonDioxide) {
	    this.carbonDioxide = carbonDioxide;
	}
	
	public int getType() {
	    return type;
	}
	
	public void setType(int type) {
	    this.type = type;
	}
}