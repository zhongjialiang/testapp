package com.jialiang.testapp.controller;
import net.sf.json.JSONObject;

import com.jialiang.testapp.entity.InfoBean;
import com.jialiang.testapp.entity.FamilyLifeQuality;

import java.util.Random;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

//import com.hyron.entity.FamilyLifeQuality;

//we usually put a class level RequestMapping so that all the mappings mentioned in this file are requested under
//same url because they usually have same responsibility.
@Controller
public class HomeController { 
    @RequestMapping(value = "/home.htm")
    //ModelAndView is one of the standard way to connect JSP file and the controller. there is also @ResponseBody
    //you can use to return object/json string  instead of a view which ModelAndView does. It's useful when
    //you are making an ajax call from front end. Take a look at here: http://docs.spring.io/spring/docs/current/spring-framework-reference/htmlsingle/#mvc-ann-responsebody
    //This page is for Sping 4, although ModelAndView and ResponseBody are not introduced from Spring4, they are very important features in Spring.
    //You can also find lots of new features on this page, I am also learning that. You can use them if you are interested in.
	public ModelAndView showMessage(@RequestParam(value = "name", required = false, defaultValue = "World") String name, ModelMap mM) 
    {
		System.out.println("in controller");
 
		ModelAndView mv = new ModelAndView("home");
		String userID = "1100";
        getQualityParameterFromDB(Integer.parseInt(userID), mM);

		return mv;
	}
    
    private void getQualityParameterFromDB(int userID,  ModelMap modelMap ) {
        InfoBean inDoorQuality = new InfoBean();
        inDoorQuality.setType(FamilyLifeQuality.in_door);
        inDoorQuality.setTemperature(new Random().nextInt() % 40);
        inDoorQuality.setHumidity(new Random().nextInt() % 20);
        inDoorQuality.setParticulateMatter(new Random().nextInt() % 100);
        inDoorQuality.setCarbonDioxide(new Random().nextInt() % 20);
        modelMap.addAttribute("indoorQuality", JSONObject.fromObject(inDoorQuality));
        
        InfoBean outDoorQuality = new InfoBean();
        outDoorQuality.setType(FamilyLifeQuality.out_door);
        outDoorQuality.setTemperature(new Random().nextInt() % 40);
        outDoorQuality.setHumidity(new Random().nextInt() % 20);
        outDoorQuality.setParticulateMatter(new Random().nextInt() % 100);
        outDoorQuality.setCarbonDioxide(new Random().nextInt() % 20);
        modelMap.addAttribute("outDoorQuality", JSONObject.fromObject(outDoorQuality));
    }
}